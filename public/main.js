 // owlCarousel nav 
$(document).ready(function () {
  $('.owl-carousel.owl-client').owlCarousel({
      loop: true,
      rtl:true,
      margin: 10,
      nav: false,
      center: false,
      responsive: {
          0: {
              items: 1
          },
          400: {
              items: 2
          },
          768: {
              items: 3
          },
          1000: {
              items: 5
          }
      }
    });
});
// owlCarousel trainers section
$(document).ready(function () {
  $('.owl-carousel.owl-carousel-trainer').owlCarousel({
      loop: true,
      rtl:true,
      // margin: 10,
      nav: false,
      center: false,
      responsive: {
          0:{
              items:1
          },
          400:{
              items:2
          },
          768:{
              items:3
          },
          1000:{
              items:4
          }

      }
  });
});
// owlCarousel stars section
$(document).ready(function () {
  $('.owl-carousel.owl-carousel-stars').owlCarousel({
      loop:true,
      rtl:true,
      autoplay: false,
      margin:10,
      center: true,
      navText:["<div class='nav-btn prev-slide'></div>","<div class='nav-btn next-slide'></div>"],
      responsiveClass:true,
      nav:false,
      responsive:{
          0:{
              items:1,
             
          },
          500:{
              items:2,
              
          },
          700:{
              items:3
          },
          1000:{
              items:4
          },
          1200:{
              items:5,
              loop:true
          }
      }
  })
  
});
// owlCarousel courses section
$(document).ready(function () {
  $('.owl-carousel-courses').owlCarousel({
      loop: false,
      rtl:true,
      margin: 10,
      nav: true,
      center: false,
      dots: false,
      autoWidth:true,
      responsive: {
          0: {
              items: 2
          },
          600: {
              items: 3
          },
          1000: {
              items: 4
          }
      }
  });
});
// owlCarousel our stars section
$(document).ready(function () {
    $('.owl-carousel-our-stars').owlCarousel({
        loop: true,
        rtl:true,
        margin: 10,
        nav: true,
        center: true,
        navText:["<div class='nav-btn prev-slide'></div>","<div class='nav-btn next-slide'></div>"],
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });
});
// owlCarousel course details page
$(document).ready(function () {
    $('.owl-carousel-course-details').owlCarousel({
        loop: true,
        rtl:true,
        margin: 10,
        nav: false,
        center: true,
        dots: false,
        // autoWidth:true,
        responsive: {
            0: {
                items: 1
            },
            500: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });
});
// owlCarousel course details page
$(document).ready(function () {
    $('.owl-carousel-about-course').owlCarousel({
        loop: true,
        margin: 10,
        rtl:true,
        nav: false,
        center: true,
        responsive: {
            0:{
                items:1
            },
            500:{
                items:2
            },
            800:{
                items:3
            }
        }
    });
});
// owlCarousel course details page  targets section
$(document).ready(function () {
    $('.owl-carousel-targets').owlCarousel({
        loop: true,
        margin: 10,
        rtl:true,
        nav: false,
        center: true,
        responsive: {
            0:{
                items:1
            },
            400:{
                items:2
            },
            800:{
                items:3
            }
        }
    });
});
// owlCarousel graduates page
$(document).ready(function () {
        $('.owl-carousel-customers').owlCarousel({
            loop: true,
            margin: 10,
            rtl:true,
            nav: false,
            center: false,
            responsive: {
                0:{
                    items:1
                },
                454:{
                    items:2
                },
                800:{
                    items:3
                },
                1000:{
                    items:4
                }
            }
        });
});
// owlCarousel graduates page
$(document).ready(function () {
    $('.owl-carousel-graduated-trainer').owlCarousel({
        loop: true,
        rtl:true,
        margin: 10,
        nav: false,
        center: true,
        responsive: {
            0:{
                items:1
            },
            500:{
                items:2
            },
            800:{
                items:3
            }
        }
    });
});
// owlCarousel blogs page
$(document).ready(function () {
    $(' .owl-carousel-blogs').owlCarousel({
        loop: true,
        margin: 30,
        rtl:true,
        nav: false,
        center: false,
        stagePadding:25,
        responsive: {
            0:{
                items:1,
                stagePadding:10,
            },
            500:{
                items:1.5,
                stagePadding:15,
            },
            600:{
                items:2
            },
            700:{
                items:2.5
            }
        }
    });
  });
  // owlCarousel main speaker page
$(document).ready(function () {
    $('.owl-carousel-speaker-trainer').owlCarousel({
        loop: true,
        margin: 10,
        rtl:true,
        nav: false,
        center: false,
        responsive: {
            0:{
                items:1
            },
            500:{
                items:2
            },
            800:{
                items:3
            },
            1000:{
                items:4
            }
        }
    });
});